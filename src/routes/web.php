<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@index')->name('contacts.index');
Route::get('/contacts/create', 'ContactController@create')->name('contacts.create')->middleware('auth');
Route::get('/contacts/{contact}', 'ContactController@show')->name('contacts.show');
Route::get('/contacts/{contact}/edit', 'ContactController@edit')->name('contacts.edit')->middleware('auth');
Route::post('/contacts', 'ContactController@store')->name('contacts.store')->middleware('auth');
Route::put('/contacts/{contact}', 'ContactController@update')->name('contacts.update')->middleware('auth');
Route::delete('/contacts/{contact}', 'ContactController@destroy')->name('contacts.destroy')->middleware('auth');

Auth::routes();
