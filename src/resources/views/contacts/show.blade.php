@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-flex flex-column flex-md-row align-items-center mb-3">
            <div class="mr-md-auto">
                <h2>@lang('View')</h2>
            </div>
            <div>
                <a class="btn btn-primary"
                   href="{{ route('contacts.index') }}">@lang('Back')</a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="name" class="control-label">@lang('Name')</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{ $contact->name }}" disabled>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="surname" class="control-label">@lang('Surname')</label>
                    <input type="text" class="form-control" name="surname" id="surname"
                           value="{{ $contact->surname }}" disabled>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="email" class="control-label">@lang('Email')</label>
                    <input type="text" class="form-control" name="email" id="email"
                           value="{{ $contact->email }}" disabled>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="phone_number" class="control-label">@lang('Phone number')</label>
                    <input type="tel" class="form-control" name="phone_number" id="phone_number"
                           value="{{ $contact->phone_number }}" disabled>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="date_of_birth" class="control-label">@lang('Date of birth')</label>
                    <input type="date" class="form-control" name="date_of_birth" id="date_of_birth"
                           value="{{ $contact->date_of_birth }}" disabled>
                </div>
            </div>
        </div>

    </div>
@endsection
