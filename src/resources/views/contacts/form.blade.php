<form id="form" role="form" method="POST" action="{{ $action }}">
    @csrf
    {{ method_field($method) }}

    <input name="id" type="hidden" value="{{ old('id', $contact->id) }}">

    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label for="name" class="control-label">@lang('Name')</label>
                <input required="required" maxlength="50" type="text"
                       class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" id="name"
                       value="{{ old('name', $contact->name) }}">
                @if ($errors->has('name'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label for="surname" class="control-label">@lang('Surname')</label>
                <input required="required" maxlength="50" type="text"
                       class="form-control {{ $errors->has('surname') ? 'is-invalid' : '' }}" name="surname"
                       id="surname"
                       value="{{ old('surname', $contact->surname) }}">
                @if ($errors->has('surname'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('surname') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label for="email" class="control-label">@lang('Email')</label>
                <input required="required" maxlength="254" type="email"
                       class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" id="email"
                       value="{{ old('email', $contact->email) }}">
                @if ($errors->has('email'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label for="phone_number" class="control-label">@lang('Phone number')</label>
                <input required="required" type="number"
                       class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" name="phone_number"
                       id="phone_number"
                       value="{{ old('phone_number', $contact->phone_number) }}">
                @if ($errors->has('phone_number'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('phone_number') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label for="date_of_birth" class="control-label">@lang('Date of birth')</label>
                <input required="required" max="{{ \Carbon\Carbon::now()->toDateString() }}" type="date"
                       class="form-control {{ $errors->has('date_of_birth') ? 'is-invalid' : '' }}" name="date_of_birth"
                       id="date_of_birth"
                       value="{{ old('date_of_birth', $contact->date_of_birth) }}">
                @if ($errors->has('date_of_birth'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <hr/>

    <div class="row">
        <div class="col-12">
            <button class="btn btn-success" type="submit">
                @if($contact->exists)
                    @lang('Update')
                @else
                    @lang('Create')
                @endif
            </button>
        </div>
    </div>
</form>
