@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-flex flex-column flex-md-row align-items-center mb-3">
            <div class="mr-md-auto">
                <h2>@lang('Edit')</h2>
            </div>
            <div>
                <a class="btn btn-primary"
                   href="{{ route('contacts.index') }}">@lang('Back')</a>
            </div>
        </div>

        @include('contacts.form', ['method' => 'PUT', 'action' => route('contacts.update', $contact->id), 'contact' => $contact])

    </div>
@endsection
