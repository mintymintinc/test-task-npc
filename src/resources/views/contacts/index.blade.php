@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-flex flex-column flex-md-row align-items-center mb-3">
            <div class="mr-md-auto">
                <h2>@lang('Contact list')</h2>
            </div>
            @auth()
                <div>
                    <a class="btn btn-success"
                       href="{{ route('contacts.create') }}">@lang('Add new')</a>
                </div>
            @endauth
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>@lang('#')</th>
                    <th>@lang('Name')</th>
                    <th>@lang('Surname')</th>
                    <th>@lang('Email')</th>
                    <th>@lang('Phone number')</th>
                    <th>@lang('Date of birth')</th>
                    <th>@lang('Actions')</th>
                </tr>
                @foreach ($contacts as $contact)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->surname }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->phone_number }}</td>
                        <td>{{ $contact->date_of_birth }}</td>
                        <td style="min-width: 206px;">
                            <a class="btn btn-secondary"
                               href="{{ route('contacts.show', ['id' => $contact->id]) }}">@lang('View')</a>
                            @auth()
                                <a class="btn btn-warning"
                                   href="{{ route('contacts.edit', ['id' => $contact->id]) }}">@lang('Edit')</a>
                                <a class="btn btn-danger" href="{{ route('contacts.destroy', ['id' => $contact->id]) }}"
                                   onclick="event.preventDefault();
                                       if(confirm('@lang('Confirm deletion!')')) {
                                       document.getElementById('delete-form-{{ $contact->id }}').submit();
                                       }
                                       ">@lang('Delete')</a>
                                <form id="delete-form-{{$contact->id}}"
                                      action="{{ route('contacts.destroy', ['id' => $contact->id]) }}"
                                      method="POST"
                                      style="display: none;">
                                    {{ method_field('DELETE') }}
                                    @csrf
                                </form>
                            @endauth
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

        {!! $contacts->links() !!}

    </div>
@endsection
