# PHP - Task # 1

## Getting Started
- Create DB
- Insert credentials to `.env` file for DB connection.
- Give proper permissions to the project directories:
	`storage` and `bootstrap/cache`
- Run command: composer install
- Run command: php artisan key:generate
- Run command: php artisan migrate

### Prerequisites
- Web Server
- MySQL Server
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Composer