<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 50);
			$table->string('surname', 50);
			$table->string('email', 254)->unique();
			$table->string('phone_number', 15);
			$table->date('date_of_birth');
		});
	}

	public function down()
	{
		Schema::drop('contacts');
	}
}
